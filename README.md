# Sceopa

### To start the app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Install [Pandoc](http://pandoc.org/)
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### To deploy to Heroku:

Sceopa runs on a single dyno with a Postgres database add-on.

The following environment variables are required:

  * SECRET_KEY_BASE - this can be generated using `mix phoenix.gen.secret`
  * SENDGRID_API_KEY - taken from a valid [Sendgrid](https://sendgrid.com) account
  * SCEOPA_ADMIN_PASSWORD - sets password for admin account

The following buildpacks are required:

  * https://github.com/heroku/heroku-buildpack-apt
  * https://github.com/HashNuke/heroku-buildpack-elixir.git
  * https://github.com/gjaldon/heroku-buildpack-phoenix-static.git

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
