# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Sceopa.Repo.insert!(%Sceopa.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
defmodule SampleData do

  alias Sceopa.Repo
  alias Sceopa.Post
  alias Sceopa.User

  def create_user(username, email, password) do
    params = %{
      "username" => username,
      "email" => email,
      "password" => password
    }
    %User{}
    |> User.registration_changeset(params)
    |> Repo.insert!()
  end

  def create_post(user, title, public, content) do
    params = %{
      "title" => title,
      "public" => public,
      "content" => content
    }
    user
    |> Ecto.build_assoc(:posts)
    |> Post.changeset(params)
    |> Repo.insert!()
  end
end

markdown_title = "Markdown test content"
markdown_content = """
## Headers

# H1
## H2
### H3
#### H4
##### H5
###### H6

## Emphasis

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

## Lists

1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses

## Links

[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself].

URLs and URLs in angle brackets will automatically get turned into links.
http://www.example.com or <http://www.example.com> and sometimes
example.com (but not on Github, for example).

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com

## Images

Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

## Code and syntax highlighting

Inline `code` has `back-ticks around` it.

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
s = "Python syntax highlighting"
print s
```

```
No language indicated, so no syntax highlighting.
But let's throw in a <b>tag</b>.
```

## Tables

Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

There must be at least 3 dashes separating each header cell.
The outer pipes (|) are optional, and you don't need to make the
raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3

## Blockquotes

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.

## Inline HTML

<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>

## Horizontal rule

Three or more...

---

Hyphens

***

Asterisks

___

Underscores

## Line breaks

Here's a line for us to start with.

This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also a separate paragraph, but...
This line is only separated by a single newline, so it's a separate line in the *same paragraph*.

## Youtube videos

<a href="http://www.youtube.com/watch?feature=player_embedded&v=YOUTUBE_VIDEO_ID_HERE
" target="_blank"><img src="http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg"
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)
"""

syntax_title = "Syntax highlighting test content"
syntax_content = """
### Haskell:

Here is a paragraph introducing the code sample. Notice that line numbers are supported.

```haskell
prompt :: [String] -> IO ()
  prompt todos = do
  putStrLn ""
  putStrLn "Current TODO list:"
  mapM_ putTodo (zip [0..] todos)
  command <- getLine
  interpret command todos
  "a very long line of source code to test wrapping works correctly even when a line is too long to fit here"
```

And here's a followup paragraph concluding this section with any other necessary info about the code example.

### Javascript:

```javascript
function secondFunction()
{
    var result;
    result = concatenate('Zara', 'Ali');
    document.write (result);
}
```

### Elixir:

```elixir
def markdown(string) do
  if !File.dir?(".temp") do
    File.mkdir ".temp"
  end
  name = ".temp/" <> "0"
  File.write name, string
  {output,_} = System.cmd "pandoc", [name , "--from=markdown" , "--to=html"]
  File.rm name
  IO.inspect output
  raw(output)
end
```
"""

admin_password = System.get_env("SCEOPA_ADMIN_PASSWORD")
admin = SampleData.create_user("sceopa", "admin@sceopa.com", admin_password)
SampleData.create_post(admin, markdown_title, false, markdown_content)
SampleData.create_post(admin, syntax_title, false, syntax_content)
