defmodule Sceopa.Mailer do
  @moduledoc """
  Bamboo email functions
  """
  use Bamboo.Mailer, otp_app: :sceopa
end
