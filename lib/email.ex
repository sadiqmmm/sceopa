defmodule Sceopa.Email do
  @moduledoc """
  Provides email sending functions.
  """
  import Bamboo.Email
  use Bamboo.Phoenix, view: Sceopa.EmailView

  alias Sceopa.Mailer

  def send_verification(user, verify_url) do
    new_email()
    |> to(user.email)
    |> from("signup@sceopa.com")
    |> subject("Sceopa.com - Email verification")
    |> assign(:verify_url, verify_url)
    |> render("verification.html")
    |> Mailer.deliver_later()
  end
end
