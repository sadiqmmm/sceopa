# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :sceopa,
  ecto_repos: [Sceopa.Repo]

# Configures the endpoint
config :sceopa, Sceopa.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "09IBWczuuGDB+8kKiQr2T5NofaGf0jfSCfeBPFhmq/6eTyGmazsG4YSGUDle/T+Y",
  render_errors: [view: Sceopa.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Sceopa.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures Bamboo for email sending
config :sceopa, Sceopa.Mailer,
  adapter: Bamboo.SendgridAdapter,
  api_key: System.get_env("SENDGRID_API_KEY")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
