defmodule Sceopa.ViewHelpers do
  @moduledoc """
  Helper functions for views.
  """

  use Phoenix.HTML

  def markdown(string) do
    temp = temp_filepath()
    File.write temp, string
    {output,_} =
      System.cmd "pandoc", [temp, "--from=markdown_github" , "--to=html"]
    File.rm temp
    raw(output)
  end

  defp temp_filepath() do
    if !File.dir?(".temp") do
      File.mkdir ".temp"
    end
    unique_val = :erlang.unique_integer([:positive])
    ".temp/#{unique_val}.txt"
  end

  def human_relative_time_from_now(date) do
    t =
      date
      |> NaiveDateTime.to_erl()
      |> :calendar.datetime_to_gregorian_seconds
    diff = :calendar.datetime_to_gregorian_seconds(:calendar.universal_time) - t
    rel_from_now(:calendar.seconds_to_daystime(diff))
  end

  defp rel_from_now({0, {0, 0, sec}}) when sec < 30,
    do: "just now"
  defp rel_from_now({0, {0, min, _}}) when min < 2,
    do: "1 minute ago"
  defp rel_from_now({0, {0, min, _}}),
    do: "#{min} minutes ago"
  defp rel_from_now({0, {1, _, _}}),
    do: "1 hour ago"
  defp rel_from_now({0, {hour, _, _}}) when hour < 24,
    do: "#{hour} hours ago"
  defp rel_from_now({1, {_, _, _}}),
    do: "1 day ago"
  defp rel_from_now({day, {_, _, _}}) when day < 0,
    do: "just now"
  defp rel_from_now({day, {_, _, _}}),
    do: "#{day} days ago"
end
