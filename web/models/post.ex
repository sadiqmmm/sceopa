defmodule Sceopa.Post do
  @moduledoc "Model and behaviour for blog posts."

  use Sceopa.Web, :model

  schema "posts" do
    field :title, :string
    field :content, :string
    field :public, :boolean
    belongs_to :user, Sceopa.User

    timestamps()
  end

  @max_title_length 200
  @max_content_length 10_000

  def changeset(model, params) do
    model
    |> cast(params, [:title, :content, :public])
    |> validate_required([:title, :content, :public])
    |> validate_length(:title, min: 1, max: @max_title_length)
    |> validate_length(:content, min: 0, max: @max_content_length)
  end

  def author?(_post, nil), do: false
  def author?(post, user) do
    post.user_id == user.id
  end

  def public?(post), do: post.public

  def visible?(post, nil), do: public?(post)
  def visible?(post, user) do
    author?(post, user) or public?(post)
  end
end
