defmodule Sceopa.User do
  @moduledoc """
  Provides user model and associated functions.
  """

  use Sceopa.Web, :model
  alias Comeonin.Bcrypt
  alias Sceopa.Email
  alias Sceopa.Repo
  alias Sceopa.Router.Helpers

  schema "users" do
    field :username, :string
    field :email, :string
    field :password, :string, virtual: true
    field :password_hash, :string

    field :email_verified, :boolean
    field :email_verification_key, :string

    has_many :posts, Sceopa.Post

    timestamps()
  end

  @username_regex ~r/^[a-zA-Z0-9]+(-?[a-zA-Z0-9]+)*$/

  def valid_username?(username) do
    if String.match?(username, @username_regex) do
      []
    else
      [
        username: """
        Username may only contain letters, digits and single hyphens. Cannot
        begin or end with a hyphen.
        """
      ]
    end
  end

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, [:username, :email])
    |> validate_required([:username, :email])
    |> validate_length(:username, min: 1, max: 20)
    |> validate_change(:username, fn _key, username ->
      valid_username?(username)
    end)
    |> unique_constraint(:username)
    |> validate_length(:email, min: 1, max: 200)
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
  end

  def registration_changeset(model, params) do
    model
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_required([:password])
    |> validate_confirmation(:password)
    |> validate_length(:password, min: 6, max: 50)
    |> put_pass_hash()
    |> put_email_key()
  end

  def username_changeset(model, params) do
    model
    |> cast(params, [:username])
    |> validate_required([:username])
    |> validate_length(:username, min: 1, max: 20)
    |> validate_change(:username, fn _key, username ->
      valid_username?(username)
    end)
  end

  def password_changeset(model, params) do
    model
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_required([:password])
    |> validate_confirmation(:password)
    |> validate_length(:password, min: 6, max: 50)
    |> put_pass_hash()
  end

  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Bcrypt.hashpwsalt(pass))
      _ ->
        changeset
    end
  end

  defp put_email_key(%Ecto.Changeset{valid?: false} = changeset), do: changeset
  defp put_email_key(changeset) do
    put_change(changeset, :email_verification_key, gen_email_key())
  end

  defp gen_email_key() do
    16
    |> :crypto.strong_rand_bytes()
    |> Base.encode16(case: :lower)
  end

  def verify_email_key(username, key) do
    case Repo.get_by(Sceopa.User, username: username) do
      user = %{email_verification_key: ^key} ->
        {:ok, user}
      _ ->
        {:error}
    end
  end

  def create_account(user_params) do
    reg_changeset = registration_changeset(%Sceopa.User{}, user_params)
    case Repo.insert(reg_changeset) do
      {:ok, user} ->
        url = gen_email_verify_url(user)
        Email.send_verification(user, url)
        {:ok, user}
      {:error, reg_changeset} -> {:error, reg_changeset}
    end
  end

  def gen_email_verify_url(user) do
    %{username: username, email: email, email_verification_key: key} = user
    Helpers.email_url(Sceopa.Endpoint, :index,
      username: username, email: email, key: key)
  end
end
