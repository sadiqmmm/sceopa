defmodule Sceopa.DashboardController do
  @moduledoc """
  Provides user dashboard behaviour.
  """

  use Sceopa.Web, :controller
  import Sceopa.Auth

  plug :authenticate

  def index(conn, _) do
    user = conn.assigns.current_user
    posts = Repo.all(assoc(user, :posts))
    render(conn, "index.html", posts: posts)
  end
end
