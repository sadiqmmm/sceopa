defmodule Sceopa.PostController do
  @moduledoc """
  Provides user dashboard behaviour.
  """

  use Sceopa.Web, :controller
  import Sceopa.Auth
  import Sceopa.PostChecks

  plug :authenticate when not action in [:show]
  plug :ensure_post_author when action in [:edit, :update, :delete]
  plug :ensure_post_visible when action in [:show]

  alias Sceopa.Post

  def new(conn, _) do
    user = conn.assigns.current_user
    changeset =
      user
      |> build_assoc(:posts)
      |> Post.changeset(%{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}) do
    user = conn.assigns.current_user
    changeset =
      user
      |> build_assoc(:posts)
      |> Post.changeset(post_params)

    case Repo.insert(changeset) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created!")
        |> redirect(to: post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, _params) do
    post =
      conn.assigns.post
      |> Repo.preload(:user)

    render(conn, "show.html", post: post, user: post.user)
  end

  def edit(conn, _params) do
    post = conn.assigns.post
    changeset = Post.changeset(post, %{})
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"post" => post_params}) do
    post = conn.assigns.post
    changeset = Post.changeset(post, post_params)

    case Repo.update(changeset) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, _params) do
    post = conn.assigns.post
    Repo.delete!(post)

    conn
    |> put_flash(:info, "Post deleted successfully.")
    |> redirect(to: dashboard_path(conn, :index))
  end
end
