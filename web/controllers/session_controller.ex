defmodule Sceopa.SessionController do
  @moduledoc """
  Provides session management behaviour.
  """

  use Sceopa.Web, :controller
  alias Sceopa.Auth
  import Sceopa.Auth

  plug :authenticate when action in [:delete]
  plug :ensure_unauthenticated when not action in [:delete]

  def new(conn, _) do
    render conn, "new.html"
  end

  def create(conn, %{"session" => %{"email" => email, "password" => pass}}) do
    case Auth.login_by_email_and_pass(conn, email, pass, repo: Repo) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "Welcome back!")
        |> redirect(to: dashboard_path(conn, :index))
      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, "Invalid email/password combination")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> Auth.logout()
    |> redirect(to: page_path(conn, :index))
  end
end
