defmodule Sceopa.UserSettingsController do
  use Sceopa.Web, :controller

  import Sceopa.Auth
  plug :authenticate

  def index(conn, _params) do
    user = conn.assigns.current_user
    render(conn, "index.html", user: user)
  end
end
