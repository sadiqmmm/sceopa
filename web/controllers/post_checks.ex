defmodule Sceopa.PostChecks do
  @moduledoc """
  Provides post-related plugs.
  """

  import Plug.Conn
  import Phoenix.Controller

  alias Sceopa.Repo
  alias Sceopa.Router.Helpers
  alias Sceopa.Post

  def ensure_post_author(conn, _opts) do
    id = conn.params["id"]
    post = Repo.get!(Post, id)
    user = conn.assigns.current_user

    if Post.author?(post, user) do
      assign(conn, :post, post)
    else
      conn
      |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end

  def ensure_post_visible(conn, _opts) do
    id = conn.params["id"]
    post = Repo.get!(Post, id)
    user = conn.assigns.current_user

    if Post.visible?(post, user) do
      assign(conn, :post, post)
    else
      conn
      |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end
end
