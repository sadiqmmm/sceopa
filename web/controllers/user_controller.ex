defmodule Sceopa.UserController do
  use Sceopa.Web, :controller

  alias Sceopa.Post
  alias Sceopa.User
  import Sceopa.Auth

  plug :authenticate when action in [:delete]
  plug :ensure_unauthenticated when not action in [:delete, :show]

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"user" => user_params}) do
    case User.create_account(user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "A confirmation email has been sent,\
         click the link inside to activate your account.")
        |> redirect(to: page_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => user_id}) do
    User
    |> Repo.get!(user_id)
    |> Repo.delete!()

    conn
    |> put_flash(:info, "Account deleted successfully.")
    |> redirect(to: page_path(conn, :index))
  end

  def show(conn, %{"id" => user_id}) do
    user =
      User
      |> Repo.get!(user_id)
      |> Repo.preload(:posts)

    current_user = conn.assigns.current_user
    posts =
      user.posts
      |> Enum.filter(&Post.visible?(&1, current_user))

    render(conn, "show.html", user: user, posts: posts)
  end
end
