defmodule Sceopa.EmailController do
  use Sceopa.Web, :controller

  alias Sceopa.Auth
  alias Sceopa.User

  def index(conn, %{"username" => username, "email" => email, "key" => key}) do
    case User.verify_email_key(username, key) do
      {:ok, user} ->
        conn
        |> Auth.login(user)
        |> put_flash(:info, "Your email '#{email}' has been verified.")
        |> redirect(to: dashboard_path(conn, :index))
      {:error} ->
        conn
        |> put_flash(:error, "Your email '#{email}' failed to verify.")
        |> redirect(to: page_path(conn, :index))
    end
  end
end
