defmodule Sceopa.Auth do
  @moduledoc """
  Provides authentication related functions.
  """

  import Plug.Conn
  alias Sceopa.Router.Helpers

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    user_id = get_session(conn, :user_id)
    user = user_id && repo.get(Sceopa.User, user_id)
    assign(conn, :current_user, user)
  end

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end

  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  def verify_pass(user, given_pass) do
    if user && checkpw(given_pass, user.password_hash) do
      {:ok, user}
    else
      {:error, :unauthorized}
    end
  end

  def login_by_email_and_pass(conn, email, given_pass, opts) do
    repo = Keyword.fetch!(opts, :repo)
    user = repo.get_by(Sceopa.User, email: email)

    cond do
      user && checkpw(given_pass, user.password_hash) ->
        {:ok, login(conn, user)}
      user ->
        {:error, :unauthorized, conn}
      true ->
        dummy_checkpw()
        {:error, :not_found, conn}
    end
  end

  import Phoenix.Controller

  def authenticated?(conn) do
    conn.assigns.current_user != nil
  end

  def authenticate(conn, _opts) do
    case authenticated?(conn) do
      true -> conn
      _ ->
        conn
        |> put_flash(:info, "You must be signed in to do that action.")
        |> redirect(to: Helpers.user_path(conn, :new))
        |> halt()
    end
  end

  def ensure_unauthenticated(conn, _opts) do
    case authenticated?(conn) do
      true ->
        conn
        |> put_flash(:info, "Only new visitors can do that action.")
        |> redirect(to: Helpers.page_path(conn, :index))
        |> halt()
      _ -> conn
    end
  end
end
