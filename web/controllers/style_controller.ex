defmodule Sceopa.StyleController do
  use Sceopa.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
