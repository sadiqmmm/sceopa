defmodule Sceopa.Router do
  use Sceopa.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Sceopa.Auth, repo: Sceopa.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Sceopa do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    get "/about", AboutController, :index

    get "/styles", StyleController, :index

    get "/users/sign_in", SessionController, :new
    post "/users/sign_in", SessionController, :create
    get "/users/sign_out", SessionController, :delete

    resources "/users", UserController, only: [:new, :create, :delete, :show]

    get "/user/settings", UserSettingsController, :index
    get "/user/password", UserSettingsPasswordController, :edit
    patch "/user/password", UserSettingsPasswordController, :update
    put "/user/password", UserSettingsPasswordController, :update
    get "/user/username", UserSettingsUsernameController, :edit
    patch "/user/username", UserSettingsUsernameController, :update
    put "/user/username", UserSettingsUsernameController, :update

    get "/email/verify", EmailController, :index

    get "/dashboard", DashboardController, :index

    resources "/posts", PostController, except: [:index]
  end

  # Other scopes may use custom stacks.
  # scope "/api", Sceopa do
  #   pipe_through :api
  # end
end
